import { BrowserRouter as Router,Route,Switch } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import Main from './components/Main';
import Questions from './components/Questions';

function App() {
  return (
    <Router>
      <div>
        <Switch>
          <Route exact path="/">
            <Main/>
          </Route>
          <Route exact path="/questions">
            <Questions/>
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
