import Button from 'react-bootstrap/Button';
import { Link } from 'react-router-dom';

const Main = ()=>{
    return(
        <div className="mt-5 text-center">
            <Link to="/questions"><Button variant="primary">Generate Business Plan</Button></Link>
        </div>
    )
}

export default Main