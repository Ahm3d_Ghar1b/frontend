const Question = ({question,name})=>(
    <div t>
        <input type="radio" name={name} value={question.value} id={question.id}/>
        <label htmlFor={question.id} className="mx-1">{question.value}</label><br/>
    </div>
)

export default Question