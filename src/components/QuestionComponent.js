import Question from "./Question"

const QuestionComponent = ({handleFirstQuestion,questions,title,name,style})=>(
    <div style={style} onChange={handleFirstQuestion} className={`mt-3 overflow-hidden`}>
        <h3>{title}</h3>
        {questions.map(question=>(
            <Question key={question.value} question={question} name={name}/>
        ))}
    </div>
)

export default QuestionComponent