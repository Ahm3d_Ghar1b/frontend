import { useState } from "react"
import QuestionComponent from "./QuestionComponent"
import Button from 'react-bootstrap/Button';
import axios from 'axios'
const Questions = ()=>{
    const [firstQuestion,setFirstQuestion]=useState('')
    const [secondQuestion,setSecondQuestion]=useState('')
    const [thirdQuestion,setThirdQuestion]=useState('')
    const [section2,setSection2]=useState(false)
    const [investment,setInvestment]=useState('')
    const [investmentAmount,setInvestmentAmount]=useState()
    const [loading,setLoading]=useState(false)
    const [submitted,setSubmitted]=useState(false)
    const Question1=[
        {
            value:'B2C',
            id:'b2c'
        },
        {
            value:'B2B',
            id:'b2b'
        },
        {
            value:'both',
            id:'both'
        }
    ]
    const Question2=[
        {
            value:'Yes',
            id:'yes'
        },
        {
            value:'No',
            id:'no'
        }
    ]
    const Question3=[
        {
            value:'Yes',
            id:'agree'
        },
        {
            value:'No',
            id:'dontagree'
        }
    ]
    const investments=[
        {
            value:'Yes',
            id:'investmentTrue'
        },
        {
            value:'No',
            id:'investmentFalse'
        }
    ]
    const handleFirstQuestion=(e)=>{
        setFirstQuestion(e.target.value)
    }
    const handleSecondQuestion=(e)=>{
        setSecondQuestion(e.target.value)
    }
    const handleThirdQuestion=(e)=>{
        setThirdQuestion(e.target.value)
    }
    const handleSection2=()=>{
        setSection2(true)
    }
    const handleInvestment=(e)=>{
        setInvestment(e.target.value)
    }
    const handleSubmit = ()=>{
      const dataStored = {
          model:firstQuestion,
          age:secondQuestion,
          industries:thirdQuestion,
          amountInvestment:investmentAmount
      }
      setLoading(true)
      axios.post('https://task-9d594-default-rtdb.firebaseio.com/data.json',dataStored).then(response=>{
        setLoading(false)
        setSubmitted(true)
      })
    }
    return(
        <div  className="container mt-5">
            <div style={section2?{height:'0px'}:null} className="overflow-hidden">
                <QuestionComponent title="Is Your Business Model B2C or B2B or Both?" 
                handleFirstQuestion={handleFirstQuestion} 
                questions={Question1}
                name='first'/>
                <QuestionComponent
                style={firstQuestion!=='B2B'&& firstQuestion!=='both'?{height:'0px'}:null}
                title="Do you target all age brackets?" 
                handleFirstQuestion={handleSecondQuestion} 
                questions={Question2}
                name='second'/>
                <QuestionComponent 
                style={firstQuestion!=='B2C'&& firstQuestion!=='both'?{height:'0px'}:null}
                title="Do you target all industries?" 
                handleFirstQuestion={handleThirdQuestion} 
                questions={Question3}
                name='third'/>
                {(firstQuestion==='B2C'&&thirdQuestion)||(firstQuestion==='B2B'&&secondQuestion)||(firstQuestion==='both'&&secondQuestion&&thirdQuestion)?<div className="mt-5 d-flex flex-row justify-content-between">
                    <div></div>
                    <Button variant="primary" onClick={handleSection2}>Next</Button>
                </div>:null}
            </div>
            <div style={!section2?{height:'0px'}:null} className="overflow-hidden">
                <QuestionComponent
                title="Did you have an investment?" 
                handleFirstQuestion={handleInvestment} 
                questions={investments}
                name='investment'/>
                <input type="number" disabled={investment==='Yes'?false:true} placeholder="Enter Your Investment Amount" className="w-100 mt-3" min="0" value={investmentAmount && Math.max(1, investmentAmount)} onChange={e => setInvestmentAmount(e.target.value ? Number(e.target.value) : e.target.value)}/>
                {investmentAmount?<div className="mt-5 d-flex flex-row justify-content-between">
                    <div></div>
                    <Button variant="primary" onClick={handleSubmit}>Submit</Button>
                </div>:null}
            </div>
            {loading?<div class="spinner-border text-primary" role="status">
                <span class="visually-hidden">Loading...</span>
            </div>:null}
            {submitted&&!loading?<div className="px-5 pt-3 pb-1 bg-primary text-white mt-5">
                <p>Your Answers are Submitted Successfully</p>
            </div>:null}
        </div>
    )
}

export default Questions